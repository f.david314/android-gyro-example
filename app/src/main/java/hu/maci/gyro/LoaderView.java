package hu.maci.gyro;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import hu.maci.gyro.element.Drawable;
import hu.maci.gyro.element.Plane;
import hu.maci.gyro.element.animator.ContextAware;
import hu.maci.gyro.element.animator.EndWaiting;
import hu.maci.gyro.element.animator.InitWaiting;
import hu.maci.gyro.element.animator.LifecycleListener;
import hu.maci.gyro.element.animator.base.AnimationStateListener;
import hu.maci.gyro.element.animator.cloud.CloudParticleSystem;
import hu.maci.gyro.element.animator.state.StateAnimation;

/**
 * Created by dfoldesi on 7/5/16.
 */
public class LoaderView extends View implements LifecycleListener {

    private List<hu.maci.gyro.element.Drawable> drawableElements;

    private long lastDrawNanosec;
    private boolean mCanceled = false;
    private AnimationStateListener mLoadingEndListener;

    public LoaderView(Context context) {
        super(context);
        init(context);
    }

    public LoaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {

//        orientation = new ImuOCfRotationMatrix(this.getContext());
//        orientation.setFilterCoefficient(0.5f);
//        orientation.reset();
//        handler = new Handler();

        lastDrawNanosec = System.nanoTime();

    }

    @Override
    public void onStart() {
        //orientation.onResume();
        mCanceled = false;

        if (drawableElements != null) {
            for (Drawable element : drawableElements) {
                if (element instanceof LifecycleListener) {
                    ((LifecycleListener) element).onStart();
                }
            }
        }
        invalidate();
        //Ezt lehet, hogy nem itt kéne!
        //handler.removeCallbacks(runnable);
    }

    @Override
    public void onPause() {
        mCanceled = true;
        if(drawableElements != null){
            for (Drawable element : drawableElements) {
                if (element instanceof LifecycleListener) {
                    ((LifecycleListener) element).onPause();
                }
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed && drawableElements == null) {
            drawableElements = new ArrayList<>();
            initDrawableElements();
            for (Drawable element : drawableElements) {

                if (element instanceof ContextAware) {
                    ((ContextAware) element).setContext(getContext());
                }

                if (element instanceof InitWaiting) {
                    ((InitWaiting) element).init();
                }
                if(element instanceof LifecycleListener){
                    ((LifecycleListener) element).onStart();
                }


                if(element instanceof Plane){
                    ((Plane) element).setEndAnimationListener(mLoadingEndListener);
                }

            }
        }
    }

    private void initDrawableElements() {
        drawableElements.add(new Plane(200, 200, getContext(), getWidth(), getHeight()));
        drawableElements.add(new CloudParticleSystem(getWidth(), getHeight()));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (drawableElements != null) {
            Paint p = new Paint();
            p.setColor(Color.BLACK);
            p.setAlpha(150);
            canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), p);

            float diffNanoTime = (System.nanoTime() - lastDrawNanosec) / 1000000000f;
            lastDrawNanosec = System.nanoTime();
            for (Drawable element : drawableElements) {
                element.draw(diffNanoTime, canvas);
            }
        }
        if (!mCanceled) invalidate();
    }


    private Bitmap loadBitmap(int resourceId) {
        Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(), resourceId);
        return icon;
    }

    public void initEndAnimation(){
        if(drawableElements != null){
            for(Drawable element : drawableElements){
                if(element instanceof EndWaiting){
                    ((EndWaiting) element).onEnd();
                }
            }
        }
    }

    public void setAnimationEndListener(AnimationStateListener loadingEndListener){
        mLoadingEndListener = loadingEndListener;
        if(drawableElements != null){
            for(Drawable element : drawableElements){
                if(element instanceof Plane){
                    ((Plane) element).setEndAnimationListener(loadingEndListener);
                }
            }
        }
    }
}
