package hu.maci.gyro;

import android.app.Activity;
import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import hu.maci.gyro.element.animator.base.AnimationStateListener;
import hu.maci.gyro.element.animator.base.AnimatorState;

public class MainActivity extends Activity{

    Handler handler;
    LoaderView loaderView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loaderView = (LoaderView)findViewById(R.id.loader);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);


        //Example
        loaderView.setAnimationEndListener(new AnimationStateListener() {
            @Override
            public void onAnimationStateEnd(AnimatorState state) {
                finish();
            }
        });

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loaderView.initEndAnimation();
            }
        },15000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loaderView.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        loaderView.onPause();
    }
}
