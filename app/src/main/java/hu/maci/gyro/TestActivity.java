package hu.maci.gyro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by dfoldesi on 7/14/16.
 */
public class TestActivity extends Activity {
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_layout);
        button = (Button)findViewById(R.id.startButton);
        button.setOnClickListener(startLoadingButtonOnClickListener);


    }

    private View.OnClickListener startLoadingButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(TestActivity.this, MainActivity.class));
        }
    };
}
