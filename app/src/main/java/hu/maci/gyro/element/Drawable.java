package hu.maci.gyro.element;

import android.graphics.Canvas;

/**
 * Created by dfoldesi on 7/6/16.
 */
public interface Drawable {
    void draw(float timeStamp,Canvas c);
}
