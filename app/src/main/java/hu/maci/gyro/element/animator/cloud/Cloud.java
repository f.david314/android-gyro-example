package hu.maci.gyro.element.animator.cloud;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hu.maci.gyro.element.animator.cloud.base.Animator;
import hu.maci.gyro.element.animator.cloud.base.BaseParticle;
import hu.maci.gyro.element.utils.FloatPoint;
import hu.maci.gyro.element.utils.RandomFactory;

/**
 * Created by dfoldesi on 7/19/16.
 */
public class Cloud implements BaseParticle{
    public final static float SPEED_MIN = 250;
    public final static float SPEED_MAX = 400;
    public final static int PART_NUMBER_MIN = 4;
    public final static int PART_NUMBER_MAX = 7;

    private FloatPoint mPosition = new FloatPoint();
    private List<CloudPart> mParts = new ArrayList<>();
    private float mSpeed;

    private Rect mStartArea;
    private Rect mEndArea;
    public Cloud(Rect startArea, Rect endArea){
        mStartArea = startArea;
        mEndArea = endArea;
    }


    public void reset(){
        mParts.clear();
        mSpeed = RandomFactory.generateRandomFloat(SPEED_MIN, SPEED_MAX);
        int partNumber = RandomFactory.generateRandomInt(PART_NUMBER_MIN,PART_NUMBER_MAX);
        //TODO: Ezt lehet, hogy optimalizálni kéne, hogy mindegyik oldalon azonosan jöjjenek a felhők
        mPosition = RandomFactory.generateRandomPointByRect(mStartArea);
        for(int i = 0;i<partNumber;i++){
            CloudPart part = new CloudPart(mPosition);
            part.setPosition(RandomFactory.generateRandomPointByRadius(mPosition, 50));
            part.setRadius(RandomFactory.generateRandomFloat(30,55));
            mParts.add(part);
        }
    }

    public boolean isEnd(){
        boolean allEndedAnimation = true;
        for(int i = 0; i< mParts.size() && allEndedAnimation; i++){
            CloudPart part = mParts.get(i);
            if(part.getCurrentAnimationIdentifier() != CloudPart.ANIMATION_IDENTIFIER_END || part.getAnimator().getCurrentState() != Animator.State.ENDED){
                allEndedAnimation = false;
            }
        }
        return allEndedAnimation;
    }



    @Override
    public void draw(float timeStamp, Canvas c) {
        for(int i = 0; i< mParts.size(); i++){
            CloudPart part = mParts.get(i);
            FloatPoint partPosition = part.position();
            if(!mStartArea.contains((int)(mPosition.x + partPosition.x),(int)(mPosition.y + partPosition.y))){
                if(part.getCurrentAnimationIdentifier() == 0){
                    part.startAnimation(CloudPart.ANIMATION_IDENTIFIER_START);
                }
                part.draw(timeStamp, c);
                if(mEndArea.contains((int)(mPosition.x + partPosition.x),(int)(mPosition.y + partPosition.y)) && part.getCurrentAnimationIdentifier() != CloudPart.ANIMATION_IDENTIFIER_END){
                    part.stopAnimation();
                    part.startAnimation(CloudPart.ANIMATION_IDENTIFIER_END);
                }
            }
        }
    }

    @Override
    public void setPosition(FloatPoint floatPoint) {
        mPosition.set(floatPoint);
    }

    @Override
    public void setSpeed(float speed) {
        mSpeed = speed;
    }

    @Override
    public FloatPoint position() {
        return mPosition;
    }

    @Override
    public float speed() {
        return mSpeed;
    }
}