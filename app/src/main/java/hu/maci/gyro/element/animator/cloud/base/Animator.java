package hu.maci.gyro.element.animator.cloud.base;

import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dfoldesi on 7/14/16.
 */
public class Animator {

    private final static Interpolator DEFAULT_INTERPOLATOR = new LinearInterpolator();
    private final static List<Interpolator> DEFAULT_INTERPOLATOR_CHAIN = new ArrayList<>();
    static{
        DEFAULT_INTERPOLATOR_CHAIN.add(DEFAULT_INTERPOLATOR);
    }

    public enum State{
        NOT_STARTED,
        RUNNING,
        ENDED
    }
    public interface AnimatorStateChange{
        void onStateChange(State animatorState);
    }

    private long mStartedTime;
    private long mDuration;
    private State mAnimatorState;
    private List<Interpolator> mInterpolators;
    private AnimatorStateChange mAnimatorStateChangeListener;
    public Animator(long animationDuration){
        mDuration = animationDuration;
        mInterpolators = new ArrayList<>();
    }
    public Animator(long animationDuration, Interpolator... interpolators){
        this(animationDuration);
        this.mInterpolators = Arrays.asList(interpolators);
    }

    public void start(){
        mStartedTime = System.nanoTime() / 1000000;
        setState(State.RUNNING);
    }
    public float getInterpolatedValue(){
        float percent = getPercent();
        if(percent == 1f){
            setState(State.ENDED);
            return 1;
        }
        float interpolatedValue = percent;
        List<Interpolator> interpolators = getInterpolators();
        for(Interpolator interpolator : interpolators){
            interpolatedValue = interpolator.getInterpolation(interpolatedValue);
        }
        return interpolatedValue;
    }

    public float getPercent(){
        if(mAnimatorState == State.ENDED) return 1f;
        float percent = ((System.nanoTime() / 1000000f) - mStartedTime) / mDuration;
        if(percent > 1) percent = 1;
        if(percent < 0) percent = 0;
        return percent;
    }
    public void setOnAnimatorStateChangeListener(AnimatorStateChange animatorStateChangeListener){
        mAnimatorStateChangeListener = animatorStateChangeListener;
    }
    public State getCurrentState(){
        return mAnimatorState;
    }



    private void setState(State state){
        mAnimatorState = state;
        if(mAnimatorStateChangeListener != null){
            mAnimatorStateChangeListener.onStateChange(state);
        }
    }
    private List<Interpolator> getInterpolators(){
        if(mInterpolators.isEmpty()){
            return DEFAULT_INTERPOLATOR_CHAIN;
        }
        return mInterpolators;
    }
}
