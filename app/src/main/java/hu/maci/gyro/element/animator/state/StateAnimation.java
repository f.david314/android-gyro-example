package hu.maci.gyro.element.animator.state;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;

import hu.maci.gyro.element.animator.ContextAware;
import hu.maci.gyro.element.animator.LifecycleListener;
import hu.maci.gyro.element.animator.RectModifier;
import hu.maci.gyro.element.animator.base.AnimationStateListener;
import hu.maci.gyro.element.animator.base.AnimatorState;
import hu.maci.gyro.element.animator.base.AnimatorSubState;

/**
 * Created by dfoldesi on 7/18/16.
 */
public class StateAnimation implements AnimatorState, RectModifier, LifecycleListener, ContextAware{

    private List<AnimatorSubState> mStates;
    private AnimatorSubState mCurrentState;
    private AnimationStateListener mAnimatorStateListener;
    private Context mContext;


    public StateAnimation(AnimatorSubState... states){
        this.mStates = new ArrayList<>();
        for(AnimatorSubState state : states){
            this.mStates.add(state);
        }
    }
    @Override
    public void draw(float timeStamp, Canvas c) {
        if(mCurrentState == null) return;
        mCurrentState.draw(timeStamp, c);
        if(mCurrentState.end(timeStamp)){
            mCurrentState.deinit();
            if(!mStates.isEmpty()){
                mCurrentState = mStates.get(0);
                mCurrentState.init();
                mStates.remove(0);
                if(mCurrentState instanceof LifecycleListener){
                    ((LifecycleListener) mCurrentState).onStart();
                }
            }else{
                if(mAnimatorStateListener != null){
                   mAnimatorStateListener.onAnimationStateEnd(this);
                }
            }
        }
    }

    @Override
    public void init() {
        if(mStates.size() > 0){
            for(AnimatorSubState state : mStates){
                if(state instanceof ContextAware){
                    ((ContextAware) state).setContext(mContext);
                }
            }

            mCurrentState = mStates.get(0);
            mCurrentState.init();
            mStates.remove(0);
        }
    }

    @Override
    public void deinit() {
        if(mCurrentState != null){
            mCurrentState.deinit();
        }
    }

    public void setOnAnimationStateChangeListener(AnimationStateListener listener){
        mAnimatorStateListener = listener;
    }
    public void addAnimatorState(AnimatorSubState state){
        mStates.add(state);
    }

    @Override
    public void modifyRect(Rect rect) {
        if(mCurrentState instanceof RectModifier){
            ((RectModifier) mCurrentState).modifyRect(rect);
        }
    }

    @Override
    public void onStart() {
        if(mCurrentState instanceof LifecycleListener){
            ((LifecycleListener) mCurrentState).onStart();
        }
    }

    @Override
    public void onPause() {
        if(mCurrentState instanceof LifecycleListener){
            ((LifecycleListener) mCurrentState).onPause();
        }
    }

    @Override
    public void setContext(Context context) {
        mContext = context;
    }
}
