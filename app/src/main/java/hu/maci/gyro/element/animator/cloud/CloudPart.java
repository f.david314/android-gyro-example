package hu.maci.gyro.element.animator.cloud;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;

import hu.maci.gyro.element.Drawable;
import hu.maci.gyro.element.animator.cloud.base.AnimatedElement;
import hu.maci.gyro.element.animator.cloud.base.Animator;
import hu.maci.gyro.element.utils.FloatPoint;
import hu.maci.gyro.element.utils.PaintFactory;

/**
 * Created by dfoldesi on 7/19/16.
 */
public class CloudPart extends AnimatedElement implements Drawable {
    public final static float RADIUS_MIN = 30;
    public final static float RADIUS_MAX = 55;

    public final static int ANIMATION_IDENTIFIER_START = 1;
    public final static int ANIMATION_IDENTIFIER_END = 2;

    private FloatPoint mPosition;
    private FloatPoint mParentPosition;
    private Paint mPartPaint;
    private float mRadius;

    public CloudPart(FloatPoint parentPosition) {
        mPosition = new FloatPoint(0, 0);
        mPartPaint = PaintFactory.white();
        mRadius = 10;
        mParentPosition = parentPosition;

        addAnimator(ANIMATION_IDENTIFIER_START, new StartAnimator());
        addAnimator(ANIMATION_IDENTIFIER_END, new EndAnimator());
    }

    public void setPosition(FloatPoint position) {
        mPosition = position;
    }
    public void setPosition(float x, float y){
        
    }

    public void setRadius(float radius) {
        mRadius = radius;
    }

    public FloatPoint position() {
        return mPosition;
    }

    public float radius() {
        CloudPartAnimator animator = (CloudPartAnimator) getAnimator();
        if (animator != null) {
            return animator.getRadius(mRadius);
        }
        return mRadius;
    }

    public Paint paint() {
        CloudPartAnimator animator = (CloudPartAnimator) getAnimator();
        if (animator == null) {
            mPartPaint.setAlpha(255);
        } else {
            mPartPaint.setAlpha(animator.getAlpha(mPartPaint.getAlpha()));
        }
        return mPartPaint;
    }


    @Override
    public void draw(float timeStamp, Canvas c) {
        c.drawCircle(mParentPosition.x + mPosition.x, mParentPosition.y + mPosition.y, radius(), paint());
    }


    private interface CloudPartAnimator {
        float getRadius(float currentRadius);

        int getAlpha(int currentAplha);
    }

    private class StartAnimator extends Animator implements CloudPartAnimator {
        public StartAnimator() {
            super(1000, new AccelerateInterpolator(10f), new OvershootInterpolator(3f));
        }

        @Override
        public float getRadius(float currentRadius) {
            return getInterpolatedValue() * currentRadius;
        }

        @Override
        public int getAlpha(int alpha) {
            return alpha;
        }
    }

    ;

    private class EndAnimator extends Animator implements CloudPartAnimator {
        public EndAnimator() {
            super(1000, new AccelerateInterpolator(10f));
        }

        @Override
        public float getRadius(float currentRadius) {
            return currentRadius - getInterpolatedValue() * currentRadius;
        }

        @Override
        public int getAlpha(int currentAplha) {
            return 255 - (int) (getInterpolatedValue() * 255);
        }
    }
}
