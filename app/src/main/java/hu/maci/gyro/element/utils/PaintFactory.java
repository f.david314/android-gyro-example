package hu.maci.gyro.element.utils;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by dfoldesi on 7/19/16.
 */
public class PaintFactory {
    public static Paint generatePaint(int color){
        Paint ret = new Paint();
        ret.setColor(color);
        return ret;
    }
    public static Paint generatePaint(int color, int alpha){
        Paint ret = new Paint();
        ret.setColor(color);
        ret.setAlpha(alpha);
        return ret;
    }

    public static Paint white(){
        return generatePaint(Color.WHITE, 255);
    }
}
