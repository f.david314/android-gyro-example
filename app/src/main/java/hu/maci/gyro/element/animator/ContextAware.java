package hu.maci.gyro.element.animator;

import android.content.Context;

/**
 * Created by dfoldesi on 7/18/16.
 */
public interface ContextAware {
    void setContext(Context context);
}
