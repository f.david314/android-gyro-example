package hu.maci.gyro.element.animator.base;

/**
 * Created by dfoldesi on 7/18/16.
 */
public interface AnimationStateListener {
    void onAnimationStateEnd(AnimatorState state);
}
