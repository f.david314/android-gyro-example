package hu.maci.gyro.element.animator;

import android.graphics.Rect;

/**
 * Created by dfoldesi on 7/18/16.
 */
public interface RectModifier {
    void modifyRect(Rect rect);
}
