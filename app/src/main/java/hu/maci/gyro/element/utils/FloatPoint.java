package hu.maci.gyro.element.utils;

import android.graphics.Rect;

/**
 * Created by dfoldesi on 7/8/16.
 */
public class FloatPoint {
    public float x;
    public float y;

    public FloatPoint(){
        x = 0;
        y = 0;
    }
    public FloatPoint(float x, float y){
        this.x = x;
        this.y = y;
    }
    public FloatPoint(FloatPoint source){
        this();
        if(source != null){
            x = source.x;
            y = source.y;
        }
    }

    public void set(FloatPoint source){
        if(source == null) return;
        this.x = source.x;
        this.y = source.y;
    }
    public FloatPoint set(float x, float y){
        this.x = x;
        this.y = y;
        return this;
    }

    public FloatPoint add(FloatPoint additional){
        if(additional == null) return this;
        this.x += additional.x;
        this.y += additional.y;
        return this;
    }


    public float length() {
        return (float) Math.sqrt(x * x + y * y);
    }
    public float distance(FloatPoint target){
        if(target == null) return 0;
        return (float)Math.sqrt( Math.pow(x - target.x,2) + Math.pow(y - target.y,2) );
    }

    public boolean contains(Rect rect){
        return rect.contains((int)x,(int)y);
    }
}
