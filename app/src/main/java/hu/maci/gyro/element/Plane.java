package hu.maci.gyro.element;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.SparseArray;

import hu.maci.gyro.R;
import hu.maci.gyro.element.animator.ContextAware;
import hu.maci.gyro.element.animator.EndWaiting;
import hu.maci.gyro.element.animator.InitWaiting;
import hu.maci.gyro.element.animator.LifecycleListener;
import hu.maci.gyro.element.animator.RectModifier;
import hu.maci.gyro.element.animator.base.AnimationStateListener;
import hu.maci.gyro.element.animator.base.AnimatorState;
import hu.maci.gyro.element.animator.state.GyroState;
import hu.maci.gyro.element.animator.state.MoveState;
import hu.maci.gyro.element.animator.state.StateAnimation;
import hu.maci.gyro.element.animator.state.WaitState;
import hu.maci.gyro.element.plane.ParticleSystem;
import hu.maci.gyro.element.utils.FloatPoint;

/**
 * Created by dfoldesi on 7/6/16.
 */
public class Plane implements Drawable, LifecycleListener, ContextAware, InitWaiting, EndWaiting {
    private final static int ANIMATION_STATE_START = 1;
    private final static int ANIMATION_STATE_END = 2;


    private final static FloatPoint LEFT_MOTOR_COORS = new FloatPoint(0.2647f, 0.4493f);
    private final static FloatPoint RIGHT_MOTOR_COORS = new FloatPoint(0.7206f, 0.4493f);

    private int canvasWidth;
    private int canvasHeight;
    private Rect canvasMargin;
    private int width;
    private int height;
    private Bitmap planeBitmap;

    private FloatPoint position;
    private Rect planeDrawRect;
    private AnimatorState currentState;
    private ParticleSystem leftSystem;
    private ParticleSystem rightSystem;

    private Context mContext;
    private SparseArray<AnimatorState> mAnimationStates;

    public void setLoadingEndState(){
        currentState.deinit();
        currentState = mAnimationStates.get(ANIMATION_STATE_END);
        currentState.init();
    }

    protected Plane(Context con, int canvasWidth, int canvasHeight) {
        mAnimationStates = new SparseArray<>();
        planeBitmap = BitmapFactory.decodeResource(con.getResources(), R.drawable.plane);
        position = new FloatPoint();
        planeDrawRect = new Rect();

        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;

        position = new FloatPoint(canvasWidth / 2, canvasHeight + 100);
    }

    public Plane(int width, int height, Context con, int canvasWidth, int canvasHeight) {
        this(con,canvasWidth, canvasHeight);
        this.width = width;
        this.height = height;

        float planeWidth = width;
        float planeHeight = height;

        leftSystem = new ParticleSystem(position, new FloatPoint(
                planeWidth * LEFT_MOTOR_COORS.x - planeWidth / 2,
                planeHeight  * LEFT_MOTOR_COORS.y - planeHeight / 2
        ));
        rightSystem = new ParticleSystem(position, new FloatPoint(
                planeWidth * RIGHT_MOTOR_COORS.x - planeWidth / 2,
                planeHeight  * RIGHT_MOTOR_COORS.y - planeHeight / 2
        ));
    }



    @Override
    public void draw(float timeStamp, Canvas c) {

        currentState.draw(timeStamp, c);
        leftSystem.draw(timeStamp, c);
        rightSystem.draw(timeStamp, c);

        calculateDrawRect();
        c.drawBitmap(planeBitmap,null, planeDrawRect, null);
    }

    private void calculateDrawRect() {
        planeDrawRect.set(
                (int) (position.x - width / 2), //Left
                (int) (position.y - height / 2), //Top
                (int) (position.x + width / 2), //Right
                (int) (position.y + height / 2) //Bottom
        );
        if(currentState instanceof RectModifier){
            ((RectModifier) currentState).modifyRect(planeDrawRect);
        }
    }

    @Override
    public void onStart() {
        if(currentState instanceof LifecycleListener){
            ((LifecycleListener) currentState).onStart();
        }
        /*leftSystem.onStart();
        rightSystem.onStart();*/
    }

    @Override
    public void onPause() {
        if(currentState instanceof LifecycleListener){
            ((LifecycleListener) currentState).onPause();
        }
        /*leftSystem.onPause();
        rightSystem.onPause();*/
    }

    @Override
    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public void init() {

        mAnimationStates.append(ANIMATION_STATE_END,new StateAnimation(
                new MoveState(position, new FloatPoint(canvasWidth / 2f, canvasHeight / 2f), 400,true,true),
                new WaitState(1f),
                new MoveState(position, new FloatPoint(canvasWidth / 2f, canvasHeight / 2f+100), 150),
                new MoveState(position, new FloatPoint(canvasWidth / 2f, -1*width), 600)
        ));
        mAnimationStates.append(ANIMATION_STATE_START,new StateAnimation(
                new MoveState(position, new FloatPoint(canvasWidth / 2, canvasHeight / 2), 400, false, true),
                new GyroState(position, canvasWidth, canvasHeight, null)
        ));


        for(int i = 0;i<mAnimationStates.size();i++){
            int key = mAnimationStates.keyAt(i);
            AnimatorState element = mAnimationStates.get(key);
            if(element instanceof ContextAware){
                ((ContextAware) element).setContext(mContext);
            }
        }

        currentState = mAnimationStates.get(ANIMATION_STATE_START);
        currentState.init();
    }

    @Override
    public void onEnd() {
        setLoadingEndState();
    }
    public void setEndAnimationListener(AnimationStateListener listener){
        AnimatorState animation = mAnimationStates.get(ANIMATION_STATE_END);
        if(animation == null || !(animation instanceof StateAnimation)){
            throw new IllegalStateException("Nem hívtad meg az init függvényt még mielőtt beállítottad volna a vége listenert!");
        }
        ((StateAnimation)animation).setOnAnimationStateChangeListener(listener);
    }
}
