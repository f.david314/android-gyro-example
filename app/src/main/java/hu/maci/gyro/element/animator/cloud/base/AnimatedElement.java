package hu.maci.gyro.element.animator.cloud.base;

import android.util.SparseArray;

/**
 * Created by dfoldesi on 7/14/16.
 */
public abstract class AnimatedElement {
    private SparseArray<Animator> mAnimators;
    private Animator mCurrentAnimator;
    private int mCurrentAnimationIdentifier;
    public AnimatedElement(){
        mAnimators = new SparseArray<>();
    }
    public void startAnimation(int animatorIdentifier){
        if(mCurrentAnimator != null || mAnimators.get(animatorIdentifier) == null){
            return;
        }
        mCurrentAnimator = mAnimators.get(animatorIdentifier);
        mCurrentAnimator.start();
        mCurrentAnimationIdentifier = animatorIdentifier;
    }
    public int getCurrentAnimationIdentifier(){
        if(mCurrentAnimator == null){
            return 0;
        }
        return mCurrentAnimationIdentifier;
    }
    public void stopAnimation(){
        if(mCurrentAnimator != null){
        }
        mCurrentAnimator = null;
    }
    protected void addAnimator(int animationIdentifier, Animator animator){
        mAnimators.append(animationIdentifier, animator);
    }
    public Animator getAnimator(){
        return mCurrentAnimator;
    }
}
