package hu.maci.gyro.element.animator.base;

import hu.maci.gyro.element.Drawable;

/**
 * Created by dfoldesi on 7/11/16.
 */
public interface AnimatorState extends Drawable {
    void init();
    void deinit();
}
