package hu.maci.gyro.element.animator.cloud.base;

import hu.maci.gyro.element.Drawable;
import hu.maci.gyro.element.utils.FloatPoint;

/**
 * Created by dfoldesi on 7/19/16.
 */
public interface BaseParticle extends Drawable {
    void setPosition(FloatPoint floatPoint);
    void setSpeed(float speed);


    FloatPoint position();
    float speed();
}
