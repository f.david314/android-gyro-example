package hu.maci.gyro.element.animator;

/**
 * Created by dfoldesi on 7/18/16.
 */
public interface LifecycleListener {
    void onStart();
    void onPause();
}
