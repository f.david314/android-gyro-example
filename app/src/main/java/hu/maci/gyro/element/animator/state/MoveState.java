package hu.maci.gyro.element.animator.state;

import android.graphics.Canvas;
import android.graphics.Rect;

import hu.maci.gyro.element.animator.RectModifier;
import hu.maci.gyro.element.animator.base.AnimatorSubState;
import hu.maci.gyro.element.utils.FloatPoint;

/**
 * Created by dfoldesi on 7/18/16.
 */
public class MoveState implements AnimatorSubState, RectModifier{
    private final static float END_THRESHOLD = 5f;

    private FloatPoint mTargetPoint;
    private FloatPoint mPositionReference;
    private FloatPoint mMoveVector;
    private float mSpeed;

    private boolean mSmoothStarting;
    private boolean mSmoothEnding;


    public MoveState(FloatPoint positionReference, FloatPoint targetPoint, float speed, boolean smootStarting, boolean smoothEnding){
        this(positionReference, targetPoint, speed);
        mSmoothStarting = smootStarting;
        mSmoothEnding = smoothEnding;
    }

    public MoveState(FloatPoint positionReference, FloatPoint targetPoint, float speed){
        mTargetPoint = targetPoint;
        mSpeed = speed;
        mPositionReference = positionReference;
        mSmoothStarting = false;
        mSmoothEnding = false;
    }

    @Override
    public boolean end(float timeStamp) {
        if(mSmoothEnding){
            return mPositionReference.distance(mTargetPoint) < END_THRESHOLD;
        }
        return mPositionReference.distance(mTargetPoint) < mSpeed * timeStamp * mMoveVector.length();
    }

    @Override
    public void deinit() {

    }

    @Override
    public void init() {
        mMoveVector = new FloatPoint((mTargetPoint.x - mPositionReference.x), (mTargetPoint.y - mPositionReference.y));
        float moveVectorLength = mMoveVector.length();
        if(moveVectorLength != 0){
            mMoveVector.set(mMoveVector.x / moveVectorLength, mMoveVector.y / moveVectorLength);
        }else{
            mMoveVector.set(0,0);
        }

    }

    @Override
    public void draw(float timeStamp, Canvas c) {
        mPositionReference.set(
                mPositionReference.x + mMoveVector.x * mSpeed * timeStamp - (!mSmoothEnding?0:(mPositionReference.distance(mTargetPoint) < 100 ? mSpeed * timeStamp * mMoveVector.x * (1 - mPositionReference.distance(mTargetPoint) / 100): 0) ),
                mPositionReference.y + mMoveVector.y * mSpeed*timeStamp - (!mSmoothEnding?0:(mPositionReference.distance(mTargetPoint) < 100 ? mSpeed * timeStamp * mMoveVector.y * (1 - mPositionReference.distance(mTargetPoint) / 100): 0) )
        );
        if(mPositionReference.distance(mTargetPoint) < END_THRESHOLD){
            mPositionReference.set(mTargetPoint);
        }
    }

    @Override
    public void modifyRect(Rect rect) {
        float additionalValue = Math.abs(mMoveVector.x * 10);
        rect.left += additionalValue;
        rect.right -= additionalValue;
    }
}
