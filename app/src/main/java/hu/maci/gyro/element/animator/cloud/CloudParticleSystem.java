package hu.maci.gyro.element.animator.cloud;

import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;

import hu.maci.gyro.element.animator.EndWaiting;
import hu.maci.gyro.element.animator.cloud.base.BaseParticle;
import hu.maci.gyro.element.animator.cloud.base.BaseParticleSystem;
import hu.maci.gyro.element.utils.RandomFactory;
import hu.maci.gyro.element.utils.Timer;

/**
 * Created by dfoldesi on 7/25/16.
 */
public class CloudParticleSystem extends BaseParticleSystem implements EndWaiting {
    public final static float SPAWN_TIME_MIN = 0.1f;
    public final static float SPAWN_TIME_MAX = 1.2f;
    private Timer mTimer;
    private  Rect mStartArea;
    private Rect mEndArea;
    private List<BaseParticle> mMarkedParticles = new ArrayList<>();
    private boolean mRunning = true;

    public CloudParticleSystem(int canvasWidth, int canvasHeight){
        super();
        mTimer = new Timer();
        mTimer.setTickTime(generateSpawnTime());

        mStartArea = new Rect(0,0,canvasWidth, 10);
        mEndArea = new Rect(0,canvasHeight - 500,canvasWidth, canvasHeight);
    }
    @Override
    protected void init() {

    }

    @Override
    protected void beforeDrawing(float timeStamp) {

        if(!mRunning) return;
        mTimer.addElapsed(timeStamp);
        if(mTimer.isElapsed()){
            mTimer.reset();
            mTimer.setTickTime(generateSpawnTime());

            Cloud c = new Cloud(mStartArea, mEndArea);
            c.reset();
            addParticle(c);
        }
    }

    @Override
    protected void afterParticleDrawing(BaseParticle particle, float timeStamp) {
        Cloud c = (Cloud)particle;
        if(c.isEnd()){
            mMarkedParticles.add(c);
        }
    }

    @Override
    protected void afterDrawing(float timeStamp) {
        if(!mMarkedParticles.isEmpty()){
            for(BaseParticle particle : mMarkedParticles){
                removeParticle(particle);
            }
            mMarkedParticles.clear();
        }
    }


    private float generateSpawnTime(){
        return RandomFactory.generateRandomFloat(SPAWN_TIME_MIN, SPAWN_TIME_MAX);
    }

    @Override
    public void onEnd() {
        mRunning = false;
    }
}
