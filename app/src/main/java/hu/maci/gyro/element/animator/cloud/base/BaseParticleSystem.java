package hu.maci.gyro.element.animator.cloud.base;

import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.List;

import hu.maci.gyro.element.Drawable;
import hu.maci.gyro.element.utils.FloatPoint;

/**
 * Created by dfoldesi on 7/19/16.
 */
public abstract class BaseParticleSystem implements Drawable{
    protected List<BaseParticle> mParticles;
    protected FloatPoint mMoveVector;

    public BaseParticleSystem(){
        mParticles = new ArrayList<>();
        mMoveVector = new FloatPoint(0,1);
    }

    protected abstract void init(); //TODO: Ezt kívülről kell majd hívni!
    protected abstract void beforeDrawing(float timeStamp);
    protected abstract void afterParticleDrawing(BaseParticle particle,float timeStamp);
    protected abstract void afterDrawing(float timeStamp);

    @Override
    public void draw(float timeStamp, Canvas c) {
        beforeDrawing(timeStamp);
        for(int i = 0;i<mParticles.size();i++){
            BaseParticle particle = mParticles.get(i);
            move(particle, timeStamp);
            particle.draw(timeStamp, c);
            afterParticleDrawing(particle, timeStamp);
        }
        afterDrawing(timeStamp);
    }

    protected void move(BaseParticle particle, float timeStamp){
        FloatPoint currentPosition = particle.position();
        currentPosition.set(
                currentPosition.x + mMoveVector.x * particle.speed() * timeStamp,
                currentPosition.y + mMoveVector.y * particle.speed() * timeStamp
        );
    }

    protected void addParticle(BaseParticle particle){
        mParticles.add(particle);
    }
    protected void removeParticle(BaseParticle particle) {
        mParticles.remove(particle);
    }
    public int getParticleCount(){
        return mParticles.size();
    }

}
