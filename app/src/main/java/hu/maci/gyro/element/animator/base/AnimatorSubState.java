package hu.maci.gyro.element.animator.base;

/**
 * Created by dfoldesi on 7/11/16.
 */
public interface AnimatorSubState extends AnimatorState{
    boolean end(float timeStamp);
}
