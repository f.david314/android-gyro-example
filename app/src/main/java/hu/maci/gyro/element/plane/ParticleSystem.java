package hu.maci.gyro.element.plane;

import hu.maci.gyro.element.animator.cloud.base.BaseParticle;
import hu.maci.gyro.element.animator.cloud.base.BaseParticleSystem;
import hu.maci.gyro.element.utils.FloatPoint;
import hu.maci.gyro.element.utils.Timer;

/**
 * Created by dfoldesi on 7/25/16.
 */
public class ParticleSystem extends BaseParticleSystem {
    public final static float LENGTH = 400f;
    public final static float LENGTH_DIFF = LENGTH / 5f;
    public final static float LENGTH_THRESHOLD = LENGTH - LENGTH_DIFF;


    private Timer mTimer;
    private FloatPoint mPosition;
    private FloatPoint mAdditionalPosition;
    private int mParticleCount;

    public ParticleSystem(FloatPoint refrencePosition, FloatPoint additionalPosition) {
        super();
        mParticleCount = (int) (LENGTH / Particle.RADIUS);
        mTimer = new Timer();
        mTimer.setTickTime((LENGTH / mMoveVector.length() / Particle.SPEED) / mParticleCount);
        mAdditionalPosition = additionalPosition;
        mPosition = refrencePosition;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void beforeDrawing(float timeStamp) {
        if(mParticleCount <= getParticleCount()) return;
        mTimer.addElapsed(timeStamp);
        if(mTimer.isElapsed()){
            Particle p = new Particle();
            resetParticle(p);
            addParticle(p);
            mTimer.reset();
        }
    }

    @Override
    protected void afterParticleDrawing(BaseParticle particle, float timeStamp) {
        Particle p = (Particle)particle;
        float particlePositionLength = p.position().length();
        p.setRadius(
                Particle.RADIUS + (Particle.RADIUS_MAX - Particle.RADIUS) * particlePositionLength / LENGTH
        );
        if(particlePositionLength > LENGTH_THRESHOLD){
            p.setAlpha((int)((255f * (LENGTH - particlePositionLength)/LENGTH_DIFF)));
            if(particlePositionLength > LENGTH){
                resetParticle(p);
            }
        }
    }

    @Override
    protected void afterDrawing(float timeStamp) {

    }

    private void resetParticle(Particle p){
        p.reset(
                (int)(mAdditionalPosition.x + mPosition.x),
                (int)(mAdditionalPosition.y + mPosition.y)
        );
    }
}
