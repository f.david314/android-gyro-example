package hu.maci.gyro.element.utils;

/**
 * Created by dfoldesi on 7/19/16.
 */
public class Timer {
    private float mElapsed = 0;
    private float mTick;

    public void addElapsed(float elapsed){
        mElapsed += elapsed;
    }

    public void setTickTime(float tickTime){
        mTick = tickTime;
    }

    public boolean isElapsed(){
        return mElapsed > mTick;
    }

    public void reset(){
        mElapsed = 0;
    }
}
