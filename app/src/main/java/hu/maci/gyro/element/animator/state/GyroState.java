package hu.maci.gyro.element.animator.state;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;

import hu.maci.gyro.element.animator.ContextAware;
import hu.maci.gyro.element.animator.LifecycleListener;
import hu.maci.gyro.element.animator.RectModifier;
import hu.maci.gyro.element.animator.base.AnimatorSubState;
import hu.maci.gyro.element.utils.FloatPoint;
import hu.maci.gyro.element.utils.gyro.ImuOCfRotationMatrix;

/**
 * Created by dfoldesi on 7/18/16.
 */
public class GyroState implements AnimatorSubState, RectModifier, LifecycleListener, ContextAware{
    private final static float SPEED = 10;
    private final static float SLOW_BORDER = 200;
    private final int SIDE_MARGIN = 200;
    private final int MARGIN_HEIGHT = 300;
    private final float MOVE_VECTOR_REFRESH_TIME = 0.2f;



    private FloatPoint mPositionReference;
    private FloatPoint mMoveVector;
    private Rect canvasMargin;
    private float mSumTime = 0f;

    private Context mContext;
    private ImuOCfRotationMatrix orientation;
    public GyroState(FloatPoint positionReference, int canvasWidth, int canvasHeight, FloatPoint moveVectorReference){
        mPositionReference = positionReference;
        canvasMargin = new Rect(SIDE_MARGIN, (canvasWidth / 2) - MARGIN_HEIGHT / 2,canvasWidth - SIDE_MARGIN, (canvasHeight / 2) + MARGIN_HEIGHT / 2);;
        mMoveVector = new FloatPoint();
    }


    @Override
    public void draw(float timeStamp, Canvas c) {
        mSumTime += timeStamp;
        if(mSumTime > MOVE_VECTOR_REFRESH_TIME){
            mSumTime = 0;
            float[] orientations = orientation.getOrientation();
            mMoveVector.set(orientations[2], orientations[1]);
        }


        float newX = mPositionReference.x + calculateSlowVector(mMoveVector.x * SPEED, canvasMargin.left, canvasMargin.right, mPositionReference.x,false);
        float newY = mPositionReference.y - calculateSlowVector(mMoveVector.y * SPEED, canvasMargin.top, canvasMargin.bottom, mPositionReference.y,true);
        if(newX > canvasMargin.right || newX < canvasMargin.left){
            newX = mPositionReference.x;
        }
        if(newY > canvasMargin.bottom || newY < canvasMargin.top){
            newY = mPositionReference.y;
        }
        mPositionReference.set(newX, newY);
    }

    @Override
    public void init() {
        orientation = new ImuOCfRotationMatrix(mContext);
        orientation.setFilterCoefficient(0.5f);
        orientation.reset();
    }

    @Override
    public boolean end(float timeStamp) {
        return false;
    }

    @Override
    public void deinit() {
        orientation.onPause();
    }

    private float calculateSlowVector(float vector, float smaller, float upper, float pos, boolean invert){
        if(smaller + SLOW_BORDER > pos && (vector < 0 && !invert || vector > 0 && invert)){
            return vector - vector*(1-Math.abs(pos - smaller)/SLOW_BORDER);
        }else if(upper - SLOW_BORDER < pos && (vector > 0 && !invert || vector < 0 && invert)){
            return vector - vector*(1-Math.abs(pos - upper)/SLOW_BORDER);
        }
        return vector;
    }

    @Override
    public void modifyRect(Rect rect) {
        float additionalValue = Math.abs(mMoveVector.x * 10);
        rect.left += additionalValue;
        rect.right -= additionalValue;
    }

    @Override
    public void onStart() {
        orientation.onResume();
    }

    @Override
    public void onPause() {
        orientation.onPause();
    }

    @Override
    public void setContext(Context context) {
        mContext = context;
    }
}
