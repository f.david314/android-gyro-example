package hu.maci.gyro.element.plane;

import android.graphics.Canvas;
import android.graphics.Paint;

import hu.maci.gyro.element.animator.cloud.base.BaseParticle;
import hu.maci.gyro.element.utils.FloatPoint;
import hu.maci.gyro.element.utils.PaintFactory;

/**
 * Created by dfoldesi on 7/25/16.
 */
public class Particle implements BaseParticle {
    public final static float RADIUS = 5;
    public final static float RADIUS_MAX = 10;
    public final static float SPEED = 100;


    private FloatPoint mPosition;
    private FloatPoint mStartPosition;
    private float mRadius;
    private Paint mPaint;

    public Particle(){
        mPosition = new FloatPoint(0,0);
        mStartPosition = new FloatPoint(0,0);
        mPaint = PaintFactory.white();
        mRadius = RADIUS;
    }

    public void reset(int startX, int startY){
        mPosition.set(0,0);
        mStartPosition.set(startX, startY);
    }

    @Override
    public void setPosition(FloatPoint floatPoint) {
        mPosition = position();
    }

    @Override
    public void setSpeed(float speed) {}

    @Override
    public FloatPoint position() {
        return mPosition;
    }

    @Override
    public float speed() {
        return SPEED;
    }

    public void setRadius(float radius){
        mRadius = radius;
    }

    public float radius(){
        return mRadius;
    }

    public Paint paint(){
        return mPaint;
    }

    public void setAlpha(int alpha){
        mPaint.setAlpha(alpha);
    }

    @Override
    public void draw(float timeStamp, Canvas c) {
        c.drawCircle(mStartPosition.x + mPosition.x, mStartPosition.y + mPosition.y, radius(),paint());
    }
}
