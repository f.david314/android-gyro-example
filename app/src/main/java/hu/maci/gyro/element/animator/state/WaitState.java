package hu.maci.gyro.element.animator.state;

import android.graphics.Canvas;

import hu.maci.gyro.element.animator.base.AnimatorSubState;

/**
 * Created by dfoldesi on 7/18/16.
 */
public class WaitState implements AnimatorSubState{
    private float mWaitTime;
    private float mCurrentWaitedTime;

    public WaitState(float waitTime){
        mWaitTime = waitTime;
    }

    @Override
    public boolean end(float timeStamp) {
        return mWaitTime< mCurrentWaitedTime;
    }

    @Override
    public void deinit() {}

    @Override
    public void init() {}

    @Override
    public void draw(float timeStamp, Canvas c) {
        mCurrentWaitedTime += timeStamp;
    }
}
