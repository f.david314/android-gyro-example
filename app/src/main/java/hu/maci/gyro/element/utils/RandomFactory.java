package hu.maci.gyro.element.utils;

import android.graphics.Rect;

import java.util.Random;

/**
 * Created by dfoldesi on 7/12/16.
 */
public class RandomFactory {
    private static Random random = new Random();
    public static FloatPoint generateRandomPointByRect(Rect rect){
        return new FloatPoint(
                random.nextFloat() * rect.width() + rect.left,
                random.nextFloat() * rect.height() + rect.top
        );
    }
    public static FloatPoint generateRandomPointByRadius(FloatPoint center, float radius){
        return new FloatPoint(
                random.nextFloat() * radius * 2 - radius + center.x,
                random.nextFloat() * radius * 2 - radius + center.y
        );
    }
    public static int generateRandomInt(){
        return random.nextInt();
    }
    public static int generateRandomInt(int max){
        return random.nextInt(max);
    }
    public static int generateRandomInt(int min,int max){
        if(min == max) return min;
        return random.nextInt(max - min) + min;
    }


    public static float generateRandomFloat(){
        return random.nextFloat();
    }
    public static float generateRandomFloat(float max){
        return random.nextFloat() * max;
    }
    public static float generateRandomFloat(float min,float max){
        return random.nextFloat() * (max - min) + min;
    }

}
